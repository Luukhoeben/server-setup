# Ubuntu LEMP stack setup script #

This script sets up your server to be a LEMP stack.
Run this script as root like this: `sudo ./server-setup.sh`

### things to do before running this script: ###

* Create your own user with sudo privileges
* Log in to your new user
* Optional: add SSH-key to your server instead of using passwords

### Creating a user: ###

Creating a new user using the root account:
```
adduser [username]
```

Add your new user to the sudoers group:
```
usermod -aG sudo [username]
```